import SessionManager from 'session-manager';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';

export default class PartnerToolsController {

    _addPartnerRepModal;

    constructor(addPartnerRepModal) {

        if (!addPartnerRepModal) {
            throw new TypeError('addPartnerRepModal required');
        }
        this._addPartnerRepModal = addPartnerRepModal;

    }

    showAddPartnerRepModal() {

        this._addPartnerRepModal
            .show();

    }

}


PartnerToolsController.$inject = [
    'addPartnerRepModal'
];
